const TelegramBot = require("node-telegram-bot-api");
const variables = require("../config/variables");

const token = variables.TOKEN_CHATBOT;
const bot = new TelegramBot(token, { polling: true });

module.exports = bot;