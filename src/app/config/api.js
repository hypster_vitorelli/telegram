const Axios = require("axios");
const variables = require("../config/variables");

const elastic = Axios.create({
    baseURL: variables.HOSTNAMEELASTIC
})

module.exports = elastic;