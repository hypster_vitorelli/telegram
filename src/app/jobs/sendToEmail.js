const cron = require("node-cron");
const Email = require("../controllers/email_services");
const Methods = require("../controllers/page_actions");

cron.schedule('0 */1 * * *', async () => {

  const data = await Methods.SendEmailFlash()

  try {
      Email.Flash(data)
  } catch (error) {
      console.log(error)
  }
});