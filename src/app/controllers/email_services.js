const FlashOrders = require("../templates/flat_orders");
const ListEmails = require("../config/email_list");
const variables = require("../config/variables");

const AWS = require("aws-sdk");

AWS.config.update({
    accessKeyId: variables.AWS_ACCESS_KEY_ID,
    secretAccessKey: variables.AWS_SECRET_ACCESS_KEY,
    region: "us-east-1"
});

class Email{
    async Flash(data){

        const ses = new AWS.SES({ apiVersion: "2020-06-08" });
        const params = {
            Destination: {
                ToAddresses: [],
                BccAddresses: ListEmails
            },
            Message: {
                Body: {
                    Html: {
                        Charset: "UTF-8",
                        Data: FlashOrders(data)
                    }
                },
                Subject: {
                    Charset: "UTF-8",
                    Data: `Flash de vendas - Sephora - ${data.data}`
                }
            },
            Source: "noreply@sephora.com.br"
        };

        const sendEmail = ses.sendEmail(params).promise();

        sendEmail.then(data => {
            console.log("email submitted to SES", data);
            return `Email enviado com sucesso`
        })
        .catch(error => {
            console.log(error);
            return `Houve um erro ao processar sua solicitação`
        });

    }
}

module.exports = new Email();