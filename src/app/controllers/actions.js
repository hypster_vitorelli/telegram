const bot = require("../config/bot_keyrus");
const Methods = require("../controllers/page_actions");
const Email = require("../controllers/email_services");

class Actions{

    async PPM(callbackQuery){

        const ppm = await Methods.getPPM()
        const msg = callbackQuery.message;

        bot.answerCallbackQuery(callbackQuery.id).then(() =>
            bot.sendMessage(
                msg.chat.id,
                `
                <b>${callbackQuery.data}</b>
                <i>_</i>
                ${callbackQuery.message.chat.first_name}, encontramos <b>${ppm.quantidade}</b> pedido(s) realizado(s).
                `,
                {
                    parse_mode: "HTML",
                }
            )
        );

    }

    async ImagemPPM(callbackQuery){

        const msg = callbackQuery.message;
        var photo = "https://lh3.googleusercontent.com/proxy/MBj95CA6jc6ffvU3THMoCF68f9PU5AKmjHrSZXLF33o0ZD0csgZf3SeXe2Ak6mr_9Em2MC5XuaDU-QD-_QLjWVZ-cc-L4grWYnIg2Z27r9NrhUEwe5UWQC8GCQHP7Xo5p8qjUo1M8ac2ZcNf7A";
        bot.sendPhoto(msg.chat.id, photo, {caption: `${callbackQuery.data}`});

    }

    async ValuePPM10(callbackQuery){

        const msg = callbackQuery.message;
        const ppm = await Methods.getPPM()

        bot.answerCallbackQuery(callbackQuery.id).then(() =>
            bot.sendMessage(
                msg.chat.id,
                `
                    *${callbackQuery.message.chat.first_name}*, Segue os valores para os últimos 10min.
                    **
                    Total: *${ppm.total}*
                `,
                {
                    parse_mode: "MARKDOWN",
                }
            )
        );

    }

    async ResumeDefaultOrders(callbackQuery){

        const resume = await Methods.getBalance24Hours();
        const msg = callbackQuery.message;

        bot.answerCallbackQuery(callbackQuery.id).then(() =>
            bot.sendMessage(
                msg.chat.id,
                `
                *${callbackQuery.data}*
                **
                **Últimos 10min**
                *${resume.ppm_10.valor} - ${resume.ppm_10.qtd} Pedido(s)*
                **
                **Hoje**
                *${resume.vendas_dia.valor} - ${resume.vendas_dia.qtd} Pedido(s)*
                **
                **Ontem**
                *${resume.vendas_ontem.valor} - ${resume.vendas_ontem.qtd} Pedido(s)*
                `,
                {
                    parse_mode: "MARKDOWN",
                }
            )
        );

    }

    async SendEmail(callbackQuery){

        const msg = callbackQuery.message;
        const data = await Methods.SendEmailFlash()

        try {

            Email.Flash(data)

            bot.answerCallbackQuery(callbackQuery.id).then(() =>
                bot.sendMessage(
                    msg.chat.id,
                    `
                    <b>${callbackQuery.data}</b>
                    <i>_</i>
                    <pre>Email(s) enviado(s) com sucesso</pre>
                    `,
                    {
                        parse_mode: "HTML",
                    }
                )
            );

        } catch (error) {

            console.log(error)

            bot.answerCallbackQuery(callbackQuery.id).then(() =>
                bot.sendMessage(
                    msg.chat.id,
                    `
                    <b>${callbackQuery.data}</b>
                    <i>_</i>
                    <pre>Houve um erro ao processar sua solicitação. Tente novamente</pre>
                    `,
                    {
                        parse_mode: "HTML",
                    }
                )
            );
        }

    }

}

module.exports = new Actions();