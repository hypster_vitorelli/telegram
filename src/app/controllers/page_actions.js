const elastic = require("../config/api");
const BRL = require("../utils/formatBRL");
const moment = require("moment");

class PageActions{

    async getPPM(req, res){

        var search_data = moment(new Date()).subtract(3, 'h').format("YYYY.MM.DD")

        const search = await elastic.get(`/sales-${search_data}/orders/_search?_source=base_subtotal,base_shipping_incl_tax,base_grand_total,created_at&size=10000`)

        var data = moment(new Date()).subtract(3, 'h').format("YYYY-MM-DD")
        var hora = moment(new Date()).subtract(3, 'h').format("HH")
        var minuto = moment(new Date()).subtract(3, 'h').format("mm")

        if(!search) return res.json({"error": "Não foi possível se conectar ao Elastic"})

        const ppm = []
        
        search.data.hits.hits.forEach(element => {

            if(element._source.created_at != null){

                var data_r = element._source.created_at.split(" ")[0]

                let item = {
                    "valor": (Number(element._source.base_subtotal).toFixed(2) * 100) / 100,
                    "frete": (Number(element._source.base_shipping_incl_tax).toFixed(2) * 100) / 100,
                    "total": (Number(element._source.base_grand_total).toFixed(2) * 100) / 100,
                    "data": element._source.created_at.split(" ")[0],
                    "hora": moment(new Date(data_r)).set({hour:Number(element._source.created_at.split(" ")[1].split(":")[0]),minute:0,second:0,millisecond:0}).subtract(3,'h').format("HH"),
                    "minuto": element._source.created_at.split(" ")[1].split(":")[1],
                    "segundo": element._source.created_at.split(" ")[1].split(":")[2]
                }
    
                ppm.push(item)
            }

        });

        const list = ppm.filter(e => e.data == data).filter(e => e.hora == hora).filter(e => (e.minuto <= minuto && e.minuto >= (minuto - 10)))

        const subtotal = list.reduce((soma, atual) => soma + atual.valor, 0);
        const total = list.reduce((soma, atual) => soma + atual.total, 0);
        const frete = list.reduce((soma, atual) => soma + atual.frete, 0);

        return {
            ppm: `10min`,
            quantidade: list.length,
            subtotal: BRL(subtotal),
            total: BRL(total),
            frete_taxa: BRL(frete)
        }

    }

    async getBalance24Hours(){

        var search_data = moment(new Date()).subtract(3, 'h').format("YYYY.MM.DD")
        var search_data_yes = moment(new Date()).subtract(1, 'd').format("YYYY.MM.DD")
        const search = await elastic.get(`/sales-${search_data}/orders/_search?_source=base_subtotal,base_shipping_incl_tax,base_grand_total,created_at&size=10000`)
        const searchNext = await elastic.get(`/sales-${search_data_yes}/orders/_search?_source=base_subtotal,base_shipping_incl_tax,base_grand_total,created_at&size=10000`)

        var dataNext = moment(new Date()).subtract(3, 'h').add(1,'d').format("YYYY-MM-DD")
        var data = moment(new Date()).subtract(3, 'h').format("YYYY-MM-DD")
        var hora = moment(new Date()).subtract(3, 'h').format("HH")
        var minuto = moment(new Date()).subtract(3, 'h').format("mm")

        if(!search) return res.json({"error": "Não foi possível se conectar ao Elastic"})

        const ppm = []
        const ppm_y = []

        search.data.hits.hits.forEach(element => {
            
            if(element._source.created_at != null){
                
                var data_r = element._source.created_at.split(" ")[0]

                let item = {
                    "valor": (Number(element._source.base_subtotal).toFixed(2) * 100) / 100,
                    "frete": (Number(element._source.base_shipping_incl_tax).toFixed(2) * 100) / 100,
                    "total": (Number(element._source.base_grand_total).toFixed(2) * 100) / 100,
                    "data": element._source.created_at.split(" ")[0],
                    "hora": moment(new Date(data_r)).set({hour:Number(element._source.created_at.split(" ")[1].split(":")[0]),minute:0,second:0,millisecond:0}).subtract(3,'h').format("HH"),
                    "minuto": element._source.created_at.split(" ")[1].split(":")[1],
                    "segundo": element._source.created_at.split(" ")[1].split(":")[2]
                }
    
                ppm.push(item)
            }

        });

        searchNext.data.hits.hits.forEach(element => {
            
            if(element._source.created_at != null){
                
                var data_r = element._source.created_at.split(" ")[0]

                let item = {
                    "valor": (Number(element._source.base_subtotal).toFixed(2) * 100) / 100,
                    "frete": (Number(element._source.base_shipping_incl_tax).toFixed(2) * 100) / 100,
                    "total": (Number(element._source.base_grand_total).toFixed(2) * 100) / 100,
                    "data": element._source.created_at.split(" ")[0],
                    "hora": moment(new Date(data_r)).set({hour:Number(element._source.created_at.split(" ")[1].split(":")[0]),minute:0,second:0,millisecond:0}).subtract(3,'h').format("HH"),
                    "minuto": element._source.created_at.split(" ")[1].split(":")[1],
                    "segundo": element._source.created_at.split(" ")[1].split(":")[2]
                }
    
                ppm_y.push(item)
            }

        });

        const n10 = ppm.filter(e => e.data == data).filter(e => e.hora == hora).filter(e => (e.minuto <= minuto && e.minuto >= (minuto - 10)))
        const nDay = ppm.filter(e => e.data == data || e.data == dataNext)
        const nYesterday = ppm_y.filter(e => e.data == moment(data).subtract(1, "d").format("YYYY-MM-DD") || e.data == data)

        const n10Total = n10.reduce((soma, atual) => soma + atual.total, 0);
        const nDayTotal = nDay.reduce((soma, atual) => soma + atual.total, 0);
        const nYesterdayTotal = nYesterday.reduce((soma, atual) => soma + atual.total, 0);

        return {
            ppm_10: {
                qtd: n10.length,
                valor: BRL(n10Total)
            },
            vendas_dia: {
                qtd: nDay.length,
                valor: BRL(nDayTotal)
            },
            vendas_ontem: {
                qtd: nYesterday.length,
                valor: BRL(nYesterdayTotal)
            }
        }

    }

    async SendEmailFlash(){
        
        var data_consult = moment(new Date()).subtract(3, 'h').format("HH")

        if(data_consult == "00"){
            var search_data = moment(new Date()).subtract(3, 'h').subtract(1,'d').format("YYYY.MM.DD")
        }else{
            var search_data = moment(new Date()).subtract(3, 'h').format("YYYY.MM.DD")
        }

        const search = await elastic.get(`/sales-${search_data}/orders/_search?_source=base_subtotal,base_shipping_incl_tax,base_grand_total,created_at&size=10000`)
        if(!search) return res.json({"error": "Não foi possível se conectar ao Elastic"})

        const ppm = []
        const documentD = []
        let hourInit = moment(new Date()).subtract(3, 'h').set({hour:0,minute:0,second:0,millisecond:0}).format("HH")

        search.data.hits.hits.forEach(element => {
    
            if(element._source.created_at != null){
                
                var data_r = element._source.created_at.split(" ")[0]

                let item = {
                    "valor": (Number(element._source.base_subtotal).toFixed(2) * 100) / 100,
                    "frete": (Number(element._source.base_shipping_incl_tax).toFixed(2) * 100) / 100,
                    "total": (Number(element._source.base_grand_total).toFixed(2) * 100) / 100,
                    "data": element._source.created_at.split(" ")[0],
                    "hora": moment(new Date(data_r)).set({hour:Number(element._source.created_at.split(" ")[1].split(":")[0]),minute:0,second:0,millisecond:0}).subtract(3,'h').format("HH"),
                    "minuto": element._source.created_at.split(" ")[1].split(":")[1],
                    "segundo": element._source.created_at.split(" ")[1].split(":")[2]
                }
    
                ppm.push(item)
            }

        });

        //DATAS PARA CONSULTA
        var data = moment(new Date()).subtract(3, 'h').format("YYYY-MM-DD")
        var data_next = moment(new Date()).subtract(3, 'h').add(1,'d').format("YYYY-MM-DD")
        var dataYes = moment(new Date()).subtract(3, 'h').subtract(1,'d').format("YYYY-MM-DD")
        //DATAS PARA CONSULTA

        var add = 0;
        for(var i = 0; i < 25; i++){
            const orders = ppm.filter(e => (e.data == data || e.data == data_next || e.data == dataYes)).filter(e => e.hora == hourInit)
            documentD.push({
                hora: moment(data).set({hour:Number(hourInit),minute:0,second:0,millisecond:0}).format("HH:mm"),
                fim: moment(data).set({hour:Number(hourInit),minute:0,second:0,millisecond:0}).add(1, 'h').format("HH:mm"),
                quantidade: orders.length,
                total: BRL(orders.reduce((soma, atual) => soma + atual.total, 0))
            })
            hourInit = moment(data).set({hour:0,minute:0,second:0,millisecond:0}).add(add, 'h').format("HH")
            add = add + 1
        }

        const relation = ppm.filter(e => e.data == data || e.data == data_next || e.data == dataYes)
        const total = relation.reduce((soma, atual) => soma + atual.total, 0);
        const quantidade = relation.length;
        const media = relation.reduce((soma, atual) => soma + atual.total, 0) / quantidade

        return { 
            total: BRL(total), 
            quantidade: quantidade, 
            media: BRL(media), 
            hoje: documentD.slice(1), 
            data: moment(new Date()).format("DD/MM/YYYY")
        }

    }

}

module.exports = new PageActions();