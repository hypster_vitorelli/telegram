const bot = require("../config/bot_keyrus");
const Actions = require("../controllers/actions");

bot.onText(/\/start/, (msg, match) => {
    bot.sendMessage(
      msg.chat.id,
      `
        Tudo certo <b><u>${msg.chat.first_name}</u></b>. Como vai?
        Digite <b><u>Menu</u></b>, para começarmos
      `,{
        parse_mode: "HTML"
      }
    );
});


bot.onText(/\Menu/, (msg, match) => {
    bot.sendMessage(
        msg.chat.id,
        `
        <i>Criamos um menu de navegação para ajudar no dia a dia.</i>
        <pre></pre>
        <i>Selecione a opção desejada</i>
        <pre></pre>
        `,
        {
        parse_mode: "HTML",
        reply_markup: {
            inline_keyboard: [
            [
                {
                text: "Qtd de pedidos (10min) - PPM",
                callback_data: "Qtd de pedidos (10min) - PPM",
                },
            ],
            [
                {
                text: "Resumo do Gráfico PPM - imagem",
                callback_data: "Resumo do Gráfico PPM - imagem",
                },
            ],
            [
                {
                text: "Valor das Vendas (10min) em R$",
                callback_data: "Valor das Vendas (10min) em R$",
                },
            ],
            [
                {
                text: "Resumo geral | Sephora Vendas",
                callback_data: "Resumo geral | Sephora Vendas",
                },
            ],
            [
                {
                text: "Relatório de vendas por Email",
                callback_data: "Relatório de vendas por Email",
                },
            ],
            ],
        },
        }
    );
});

bot.on("callback_query", async (callbackQuery) => {

    if(callbackQuery.data === "Qtd de pedidos (10min) - PPM"){
        await Actions.PPM(callbackQuery);
    }

    if(callbackQuery.data === "Resumo do Gráfico PPM - imagem"){
        await Actions.ImagemPPM(callbackQuery);
    }

    if(callbackQuery.data === "Valor das Vendas (10min) em R$"){
        await Actions.ValuePPM10(callbackQuery);
    }

    if(callbackQuery.data === "Resumo geral | Sephora Vendas"){
        await Actions.ResumeDefaultOrders(callbackQuery);
    }

    if(callbackQuery.data === "Relatório de vendas por Email"){
        await Actions.SendEmail(callbackQuery);
    }

});