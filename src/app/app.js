require("dotenv/config");
const path = require("path");
const cors = require("cors");
const bodyParser = require("body-parser");
const express = require("express");
const routes = require("./routes");
require("./telegram/app");
require("./jobs/sendToEmail");

class App {
  constructor() {
    this.server = express();
    this.middlewares();
    this.routes();
  }
  middlewares() {
    this.server.use(cors());
    this.server.set("view engine", "ejs");
    this.server.set("views", path.resolve(__dirname, "views"));
    this.server.use(express.json());
    this.server.use(
      bodyParser.json({
        type: "application/*+json",
        limit: "50mb",
      })
    );
    this.server.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
    this.server.use(
      "/static",
      express.static(path.resolve(__dirname, "..", "..", "static"))
    );
  }
  routes() {
    this.server.use(routes);
  }
}

module.exports = new App().server;
